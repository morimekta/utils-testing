package net.morimekta.testing.junit4;

import net.morimekta.io.tty.TTYSize;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.Description;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConsoleWatcherTest {
    @Rule
    public ConsoleWatcher consoleWatcher = new ConsoleWatcher()
            .dumpErrorOnFailure()
            .dumpOutputOnFailure()
            .dumpOnFailure()
            .nonInteractive()
            .interactive()
            .withTerminalSize(20, 80);

    @Test
    public void testSimpleOutput() {
        var console = consoleWatcher.console();

        System.err.println("foo");
        System.out.println("bar");
        assertThat(console.error(), is("foo\n"));
        assertThat(console.output(), is("bar\n"));
        assertThat(console.tty().isInteractive(), is(true));
        assertThat(consoleWatcher.tty().getTerminalSize(), is(new TTYSize(20, 80)));
        assertThat(console.tty().getTerminalSize(), is(new TTYSize(20, 80)));
    }

    @Test
    public void testTestOverrides() {
        consoleWatcher.withTerminalSize(24, 144);
        consoleWatcher.interactive();
        consoleWatcher.dumpOnFailure();
        // TODO: Test with different defaults.
    }

    @Test
    public void testTestFailure() {
        consoleWatcher.failed(new IOException(), Description.EMPTY);
        // TODO: verify output.
    }
}
