package net.morimekta.testing.junit4;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static java.util.Arrays.asList;
import static net.morimekta.testing.junit4.DataProviderUtil.buildDataDimensions;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

@RunWith(DataProviderRunner.class)
public class DataProviderUtilTest {
    @Test
    public void testBuildDataDimension() {
        Object[][] out = buildDataDimensions(
                asList(1, 2, 3),
                asList("a", "b"),
                asList('y', 'n', '?'));
        assertThat(out.length, is(3 * 2 * 3));
        for (Object[] a : out) {
            assertThat(a.length, is(3));
            assertThat(a[0], is(instanceOf(Integer.TYPE)));
            assertThat(a[1], is(instanceOf(String.class)));
            assertThat(a[2], is(instanceOf(Character.TYPE)));
        }
        // the first is the first of all
        assertThat(out[0], is(new Object[]{1, "a", 'y'}));
        // the last is the last of all
        assertThat(out[17], is(new Object[]{3, "b", '?'}));

        try {
            buildDataDimensions();
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No dimensions provided"));
        }

        try {
            buildDataDimensions(new ArrayList<>());
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty dimension in layer 1"));
        }

        try {
            buildDataDimensions(asList(1, 2), new ArrayList<>());
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty dimension in layer 2"));
        }
    }

    @DataProvider
    public static Object[][] dataProvider() {
        return buildDataDimensions(
                List.of("a", "A"),
                List.of("b", "B"));
    }

    @Test
    @UseDataProvider("dataProvider")
    public void testProvidedValues(String a, String b) {
        assertThat(a.toLowerCase(Locale.US), is("a"));
        assertThat(b.toUpperCase(Locale.US), is("B"));
    }
}
