/**
 * Module with junit 5 testing utilities.
 */
module net.morimekta.testing.junit4 {
    exports net.morimekta.testing.junit4;

    requires transitive net.morimekta.testing;
    requires transitive net.morimekta.io;
    requires transitive junit;
}