Testing Utilities
=================

[![Morimekta](https://img.shields.io/static/v1?label=morimekta.net&message=utils-testing&color=informational)](https://morimekta.net/utils-testing/)
[![Docs](https://www.javadoc.io/badge/net.morimekta.utils/testing.svg)](https://www.javadoc.io/doc/net.morimekta.utils/testing)
[![Pipeline](https://gitlab.com/morimekta/utils-testing/badges/master/pipeline.svg)](https://gitlab.com/morimekta/utils-testing/pipelines)
[![Coverage](https://gitlab.com/morimekta/utils-testing/badges/master/coverage.svg)](https://morimekta.net/utils-testing/jacoco-aggregate/)
[![License](https://img.shields.io/static/v1?label=license&message=apache%202.0&color=informational)](https://apache.org/licenses/LICENSE-2.0)  
Various Utilities used in testing. This module contains a number of utilities
and helpers to help with testing. It consists of a number of mostly independent
parts.

See [morimekta.net/utils](https://morimekta.net/utils/) for procedures on releases.

## Getting Started

To add to `maven`: Add this line to `pom.xml` under dependencies:

```xml
<dependencies>
    <dependency>
        <groupId>net.morimekta.utils</groupId>
        <artifactId>testing</artifactId>
        <version>${version}</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>net.morimekta.utils</groupId>
        <artifactId>testing-junit4</artifactId>
        <version>${version}</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>net.morimekta.utils</groupId>
        <artifactId>testing-junit5</artifactId>
        <version>${version}</version>
        <scope>test</scope>
    </dependency>
</dependencies>
```

To add to `gradle`: Add this line to the `dependencies` group in `build.gradle`:

```
testImplementation 'net.morimekta.utils:testing:${version}'
testImplementation 'net.morimekta.utils:testing-junit4:${version}'
testImplementation 'net.morimekta.utils:testing-junit5:${version}'
```

## Testing

General testing utilities.

### Concurrency Utils

- `FakeClock`: A fake java.time.Clock that keeps constant time and is ticked
  manually.
- `FakeScheduledExecutor`: A scheduled executor that uses a fake clock
  and 'ticking' of that to trigger scheduled tasks. Using a real executor in
  the background.
- `ImmediateExecutor`: A fake executor that executes tasks when they are
  submitted. This means they run while waiting for `submit` to return, so
  all tasks are completed immediately.
- `ImmediateScheduledExecutor`: A fake scheduled executor that uses a fake
  clock and 'ticking' of that to trigger scheduled tasks in the same thread
  as triggers the tick. Submitted tasks are always handled in the `tick`
  listener.

### Console Utils

- `Console`: An interface to interact with a console during testing.
- `ConsoleManager`: Base class for managing a fake console during testing that
  will capture whats written to standard OUT and ERR and replace standard IN.

### I/O and File Utils

- `ResourceUtil`: Some simple utils for getting hand on resource content.
- `TestConfigMap`: A class wrapping around a directory to make it behave as a
  [kubernetes](https://kubernetes.io) ConfigMap.

### Text Utils

- `EnglishWords`: A super-light text and word generator.

## JUnit 4

Utilities specialized for JUnit 4.

- `ConsoleWatcher`: A junit4 `@Rule` to set up and reset the console.
- `DataProviderUtil`: Utility for generating matrices of arguments to from a data provider using
  `com.tngtech.java:junit-dataprovider`.

## JUnit 5

Utilities specialized for JUnit 5.

- `ConsoleExtension`: A junit5 extension to set up and reset the console,
  and give access to `Console` and `TTY` parameters.
- `ParamsProviderUtil`: Utility for generating matrices of arguments to from a params provider.
- `DataSourceExtension`: A junit 5 extension to set up a JDBC data source to be used in testing.

## Major Version Changes

- `4.x` -> `5.x`: FakeScheduledExecutor renamed ImmediateScheduledExecutor and
  new FakeScheduledExecutor created using a real background executor, but using
  the fake clock to trigger the scheduled tasks.
