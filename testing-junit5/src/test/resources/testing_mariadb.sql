--
-- The content categories. Not auto-increment of id, we want them to be under control.
--
CREATE SCHEMA IF NOT EXISTS `testing`;
USE `testing`;

CREATE TABLE `the_first`
(
    id    INT          NOT NULL AUTO_INCREMENT,
    name  VARCHAR(255) NOT NULL,
    ctype INT          NOT NULL,

    PRIMARY KEY (ctype, id)
);
