--
-- The content categories. Not auto-increment of id, we want them to be under control.
--
CREATE SCHEMA IF NOT EXISTS `testing`;
USE `testing`;

    CREATE TABLE `testing`.`the_first`
(
    id    SERIAL       NOT NULL,
    name  VARCHAR(255) NOT NULL,
    ctype INT          NOT NULL,

    CONSTRAINT pk_content PRIMARY KEY (ctype, id)
);