module net.morimekta.testing.junit5.test {
    exports net.morimekta.testing.junit5.test;
    exports net.morimekta.testing.junit5.test.sql;

    requires net.morimekta.testing;
    requires net.morimekta.file;
    requires net.morimekta.io;

    requires org.junit.jupiter.params;
    requires org.junit.jupiter.api;
    requires org.junit.jupiter.engine;
    requires org.hamcrest;

    requires org.slf4j;
    requires ch.qos.logback.classic;
    requires ch.qos.logback.core;

    requires java.sql;
    requires net.morimekta.testing.junit5;
    requires org.jdbi.v3.core;
    requires org.jdbi.v3.sqlobject;
    requires com.h2database;
    requires tomcat.jdbc;
}
