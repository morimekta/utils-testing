package net.morimekta.testing.junit5.test;

import net.morimekta.io.tty.TTY;
import net.morimekta.io.tty.TTYSize;
import net.morimekta.testing.console.Console;
import net.morimekta.testing.junit5.ConsoleExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(ConsoleExtension.class)
public class ConsoleExtensionTest {
    @Test
    public void testDefaults(Console console, TTY tty, TestInfo info) {
        assertThat(info.getDisplayName(), is("testDefaults(Console, TTY, TestInfo)"));
        assertThat(tty.isInteractive(), is(true));
        assertThat(tty.getTerminalSize(), is(new TTYSize(25, 144)));

        System.err.println("foo");
        System.out.println("bar");
        assertThat(console.error(), is("foo\n"));
        assertThat(console.output(), is("bar\n"));
    }
}
