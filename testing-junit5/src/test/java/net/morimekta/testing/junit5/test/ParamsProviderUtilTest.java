package net.morimekta.testing.junit5.test;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.morimekta.testing.junit5.ParamsProviderUtil.buildArgumentDimensions;
import static org.hamcrest.CoreMatchers.either;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ParamsProviderUtilTest {
    @Test
    public void testBuildArgumentDimensions() {
        Stream<Arguments> out = buildArgumentDimensions(
                arguments(1, 2, 3),
                arguments("a", "b"),
                arguments('y', 'n', '?'));
        int len = 0;
        for (Arguments a : out.collect(Collectors.toList())) {
            if (len == 0) {
                assertThat(a.get(), is(new Object[]{1, "a", 'y'}));
            } else if (len == (3 * 2 * 3 - 1)) {
                assertThat(a.get(), is(new Object[]{3, "b", '?'}));
            }
            ++len;
            assertThat(a.get().length, is(3));
            assertThat(a.get()[0], is(instanceOf(Integer.TYPE)));
            assertThat(a.get()[1], is(instanceOf(String.class)));
            assertThat(a.get()[2], is(instanceOf(Character.TYPE)));
        }
        assertThat(len, is(3 * 2 * 3));

        try {
            buildArgumentDimensions();
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No dimensions provided"));
        }

        try {
            buildArgumentDimensions(arguments());
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty dimension in layer 1"));
        }

        try {
            buildArgumentDimensions(arguments(1, 2), arguments());
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty dimension in layer 2"));
        }
    }

    public static Stream<Arguments> params() {
        return buildArgumentDimensions(
                arguments("foo", "bar", "baz"),
                arguments(Locale.GERMANY, Locale.US, Locale.CHINA));
    }

    @ParameterizedTest
    @MethodSource("params")
    public void testParams(String name, Locale locale) {
        assertThat(name, either(is("foo")).or(is("bar")).or(is("baz")));
        assertThat(locale, either(is(Locale.GERMANY)).or(is(Locale.US)).or(is(Locale.CHINA)));
    }
}
