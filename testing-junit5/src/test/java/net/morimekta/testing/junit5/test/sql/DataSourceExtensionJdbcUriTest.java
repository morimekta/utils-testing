package net.morimekta.testing.junit5.test.sql;

import net.morimekta.file.FileUtil;
import net.morimekta.testing.junit5.sql.DataSourceDriverClass;
import net.morimekta.testing.junit5.sql.DataSourceExtension;
import net.morimekta.testing.junit5.sql.DataSourceSchema;
import net.morimekta.testing.junit5.sql.DataSourceURI;
import net.morimekta.testing.junit5.sql.DataSourceURIMethod;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.h2.Driver;
import org.h2.tools.Server;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.h2.H2DatabasePlugin;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(DataSourceExtension.class)
@DataSourceSchema("/testing_mysql.sql")
@DataSourceURIMethod("getJdbcUri")
public class DataSourceExtensionJdbcUriTest {
    // for printing
    private Path   databaseDir;
    private Server h2Server;
    private String jdbcUri;

    @SuppressWarnings("unused")
    public String getJdbcUri() throws IOException, SQLException {
        if (h2Server == null) {
            this.databaseDir = Files.createTempDirectory("junit-h2");
            this.h2Server = Server.createTcpServer(
                    "-tcpPort", "0",
                    "-tcp",
                    "-tcpAllowOthers",
                    "-tcpDaemon",
                    "-ifNotExists");
            this.h2Server.setOut(System.err);
            this.h2Server.start();
        }
        jdbcUri = "jdbc:h2:" + h2Server.getURL() + "/" + databaseDir.toString() + ";MODE=MYSQL";
        return jdbcUri;
    }

    @AfterEach
    public void tearDown() throws IOException {
        if (h2Server != null) {
            h2Server.shutdown();
            h2Server.stop();
            h2Server = null;
        }
        if (databaseDir != null) {
            if (Files.exists(databaseDir)) {
                FileUtil.deleteRecursively(databaseDir);
            }
        }
    }

    @Test
    public void testMySQL(Jdbi jdbi) throws SQLException, IOException {
        var dbi = jdbi.onDemand(TestDBI.class);
        assertThat(dbi.getName(0, 2), is(nullValue()));
        var id = dbi.setName(2, "foo");
        assertThat(dbi.getName(id, 2), is("foo"));

        var dbConfig = new PoolProperties();
        dbConfig.setUrl(getJdbcUri());
        dbConfig.setDriverClassName(Driver.class.getName());
        dbConfig.setInitialSize(2);
        dbConfig.setMinIdle(2);
        dbConfig.setMaxIdle(10);
        dbConfig.setMaxActive(10);
        dbConfig.setValidationQuery("SELECT 1");
        dbConfig.setTestOnBorrow(true);
        dbConfig.setTestOnReturn(false);
        dbConfig.setValidationInterval(60);
        dbConfig.setSuspectTimeout(300);
        dbConfig.setLogAbandoned(true);

        var ds = new org.apache.tomcat.jdbc.pool.DataSource(dbConfig);
        try {
            var db = Jdbi.create(ds);
            db.installPlugin(new H2DatabasePlugin());
            db.installPlugin(new SqlObjectPlugin());
            dbi = db.onDemand(TestDBI.class);
            assertThat(dbi.getName(id, 2), is("foo"));
        } finally {
            ds.close();
        }
    }

    @Test
    @DataSourceDriverClass(Driver.class)
    public void testJdbcUri(@DataSourceURI String uri,
                            @TempDir Path tmp) {
        assertThat(uri, is(jdbcUri));
        assertThat(tmp, is(notNullValue()));
    }
}
