package net.morimekta.testing.junit5.test.sql;

import net.morimekta.testing.junit5.sql.DataSourceExtension;
import net.morimekta.testing.junit5.sql.DataSourceMode;
import net.morimekta.testing.junit5.sql.DataSourceSchema;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(DataSourceExtension.class)
public class DataSourceExtensionTest {
    @Test
    @DataSourceSchema("/testing_mysql.sql")
    @DataSourceMode(DataSourceMode.Mode.MYSQL)
    public void testMySQL(Jdbi jdbi) {
        var dbi = jdbi.onDemand(TestDBI.class);
        assertThat(dbi.getName(0, 2), is(nullValue()));
        var id = dbi.setName(2, "foo");
        assertThat(dbi.getName(id, 2), is("foo"));
    }

    @Test
    @DataSourceSchema("/testing_postgresql.sql")
    @DataSourceMode(DataSourceMode.Mode.POSTGRESQL)
    public void testPostgreSQL(Jdbi jdbi) {
        var dbi = jdbi.onDemand(TestDBI.class);
        assertThat(dbi.getName(0, 2), is(nullValue()));
        var id = dbi.setName(2, "foo");
        assertThat(dbi.getName(id, 2), is("foo"));
    }

    @Test
    @DataSourceSchema("/testing_mariadb.sql")
    @DataSourceMode(DataSourceMode.Mode.MARIADB)
    public void testMariaDb(Jdbi jdbi) {
        var dbi = jdbi.onDemand(TestDBI.class);
        assertThat(dbi.getName(0, 2), is(nullValue()));
        var id = dbi.setName(2, "foo");
        assertThat(dbi.getName(id, 2), is("foo"));
    }
}
