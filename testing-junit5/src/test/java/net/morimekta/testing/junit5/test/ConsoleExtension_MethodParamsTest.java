package net.morimekta.testing.junit5.test;

import net.morimekta.io.tty.TTY;
import net.morimekta.testing.console.Console;
import net.morimekta.testing.junit5.ConsoleDumpErrorOnFailure;
import net.morimekta.testing.junit5.ConsoleDumpOutputOnFailure;
import net.morimekta.testing.junit5.ConsoleExtension;
import net.morimekta.testing.junit5.ConsoleInteractive;
import net.morimekta.testing.junit5.ConsoleSize;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(ConsoleExtension.class)
public class ConsoleExtension_MethodParamsTest {
    @Test
    @ConsoleSize(cols = 80, rows = 20)
    @ConsoleDumpErrorOnFailure
    @ConsoleDumpOutputOnFailure
    public void testSimpleOutput(Console console) {
        System.err.println("foo");
        System.out.println("bar");
        assertThat(console.error(), is("foo\n"));
        assertThat(console.output(), is("bar\n"));
    }

    @Test
    @ConsoleInteractive(false)
    public void testDefaults(Console console, TTY tty) {
        assertThat(tty.isInteractive(), is(false));

        System.err.println("foo");
        System.out.println("bar");
        assertThat(console.error(), is("foo\n"));
        assertThat(console.output(), is("bar\n"));
    }
}
