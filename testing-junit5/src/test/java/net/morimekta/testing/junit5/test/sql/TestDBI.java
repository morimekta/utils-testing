package net.morimekta.testing.junit5.test.sql;

import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

public interface TestDBI {
    @SqlQuery("SELECT name FROM `testing`.`the_first` WHERE id = ? AND ctype = ?")
    String getName(int id, int ctype);

    @SqlUpdate("INSERT INTO `testing`.`the_first` (ctype, name) VALUES (?, ?)")
    int setName(int ctype, String name);
}
