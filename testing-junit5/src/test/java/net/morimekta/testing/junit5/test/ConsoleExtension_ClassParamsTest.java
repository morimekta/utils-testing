package net.morimekta.testing.junit5.test;

import net.morimekta.testing.console.Console;
import net.morimekta.testing.junit5.ConsoleDumpErrorOnFailure;
import net.morimekta.testing.junit5.ConsoleDumpOutputOnFailure;
import net.morimekta.testing.junit5.ConsoleExtension;
import net.morimekta.testing.junit5.ConsoleInteractive;
import net.morimekta.testing.junit5.ConsoleSize;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(ConsoleExtension.class)
@ConsoleSize(cols = 80, rows = 20)
@ConsoleInteractive(true)
@ConsoleDumpErrorOnFailure
@ConsoleDumpOutputOnFailure
public class ConsoleExtension_ClassParamsTest {
    @Test
    public void testSimpleOutput(Console console) {
        System.err.println("foo");
        System.out.println("bar");
        assertThat(console.error(), is("foo\n"));
        assertThat(console.output(), is("bar\n"));
    }
}
