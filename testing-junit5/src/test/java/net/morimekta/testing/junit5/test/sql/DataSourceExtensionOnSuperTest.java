package net.morimekta.testing.junit5.test.sql;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;

public class DataSourceExtensionOnSuperTest extends DataSourceExtensionOnClassTest {
    @Disabled
    @Override
    public void testMySQL(DataSource ds) {}

    @Test
    public void testMySQL2(DataSource ds) {
        super.testMySQL(ds);
    }
}
