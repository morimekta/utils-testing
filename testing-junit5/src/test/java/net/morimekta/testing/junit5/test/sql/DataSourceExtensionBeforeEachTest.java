package net.morimekta.testing.junit5.test.sql;

import net.morimekta.testing.junit5.sql.DataSourceExtension;
import net.morimekta.testing.junit5.sql.DataSourceMode;
import net.morimekta.testing.junit5.sql.DataSourceSchema;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(DataSourceExtension.class)
@DataSourceSchema("/testing_mysql.sql")
@DataSourceMode(DataSourceMode.Mode.MYSQL)
public class DataSourceExtensionBeforeEachTest {
    private Jdbi jdbi;

    @BeforeEach
    public void setUp(Jdbi jdbi) {
        this.jdbi = jdbi;
    }

    @Test
    public void testMySQL() {
        var dbi = jdbi.onDemand(TestDBI.class);
        assertThat(dbi.getName(0, 2), is(nullValue()));
        var id = dbi.setName(2, "foo");
        assertThat(dbi.getName(id, 2), is("foo"));
    }

    @Test
    public void testPostgreSQL() {
        var dbi = jdbi.onDemand(TestDBI.class);
        assertThat(dbi.getName(0, 2), is(nullValue()));
        var id = dbi.setName(2, "foo");
        assertThat(dbi.getName(id, 2), is("foo"));
    }
}
