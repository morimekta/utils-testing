package net.morimekta.testing.junit5.test.sql;

import net.morimekta.testing.junit5.sql.DataSourceExtension;
import net.morimekta.testing.junit5.sql.DataSourceMode;
import net.morimekta.testing.junit5.sql.DataSourceSchema;
import net.morimekta.testing.junit5.sql.DataSourceURI;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.h2.H2DatabasePlugin;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.sql.DataSource;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(DataSourceExtension.class)
@DataSourceSchema("/testing_mysql.sql")
@DataSourceMode(DataSourceMode.Mode.MYSQL)
public class DataSourceExtensionOnClassTest {
    @DataSourceURI
    public String uri;

    @Test
    public void testMySQL(DataSource ds) {
        var jdbi = Jdbi.create(ds);
        jdbi.installPlugin(new H2DatabasePlugin());
        jdbi.installPlugin(new SqlObjectPlugin());

        var dbi = jdbi.onDemand(TestDBI.class);
        assertThat(dbi.getName(0, 2), is(nullValue()));
        var id = dbi.setName(2, "foo");
        assertThat(dbi.getName(id, 2), is("foo"));
        assertThat(uri, is(((org.apache.tomcat.jdbc.pool.DataSource) ds).getUrl()));
    }
}
