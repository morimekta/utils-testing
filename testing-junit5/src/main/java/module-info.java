/**
 * Module with junit 5 testing utilities.
 */
module net.morimekta.testing.junit5 {
    exports net.morimekta.testing.junit5;
    exports net.morimekta.testing.junit5.sql;

    requires transitive net.morimekta.testing;
    requires net.morimekta.file;
    requires transitive net.morimekta.io;
    requires transitive org.junit.jupiter.api;
    requires transitive org.junit.jupiter.params;

    requires com.h2database;
    requires transitive java.sql;
    requires java.management;
    requires org.jdbi.v3.core;
    requires org.jdbi.v3.sqlobject;
    requires tomcat.jdbc;
}
