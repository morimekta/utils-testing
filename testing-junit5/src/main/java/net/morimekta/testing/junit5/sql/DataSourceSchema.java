package net.morimekta.testing.junit5.sql;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Specify SQL schemas to be loaded on startup of the
 * DB, before starting the test. Does not require use of the
 * H2 DB.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface DataSourceSchema {
    /**
     * @return Resources with SQL to run in DB startup. In order of execution.
     *         If defined on multiple levels will be in order of (super-class,
     *         class, method) and in declared order within each group.
     */
    String[] value();
}
