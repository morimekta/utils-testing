package net.morimekta.testing.junit5.sql;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * If no {@link DataSourceURIMethod} is specified to connect to an
 * existing DB instance, then create a H2 in-memory DB instance using
 * a specified query syntax mode.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface DataSourceMode {
    Mode value();

    enum Mode {
        /**
         * Use MySQL query syntax and nits.
         */
        MYSQL("MySQL;DATABASE_TO_LOWER=TRUE"),
        /**
         * Use MariaDB query syntax and nits.
         */
        MARIADB("MariaDB;DATABASE_TO_LOWER=TRUE"),
        /**
         * Use PostgreSQL query syntax and nits.
         */
        POSTGRESQL("PostgreSQL;DATABASE_TO_LOWER=TRUE;DEFAULT_NULL_ORDERING=HIGH"),
        ;

        final String mode;

        Mode(String mode) {
            this.mode = mode;
        }
    }
}
