package net.morimekta.testing.junit5;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * When using the {@link ConsoleExtension}, this will trigger its TTY to behave as an interactive console
 * based on the provided value. Default is to be interactive.
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface ConsoleInteractive {
    /**
     * @return If the console should be interactive.
     */
    boolean value();
}
