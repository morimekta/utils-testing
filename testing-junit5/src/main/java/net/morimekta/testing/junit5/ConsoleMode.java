package net.morimekta.testing.junit5;

import net.morimekta.io.tty.TTYMode;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * When using the {@link ConsoleExtension}, this will override the provided console size. Default
 * is 144 cols by 25 rows.
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface ConsoleMode {
    /**
     * @return Number of characters rows.
     */
    TTYMode value() default TTYMode.COOKED;
}
