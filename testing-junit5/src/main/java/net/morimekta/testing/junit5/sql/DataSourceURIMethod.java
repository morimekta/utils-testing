package net.morimekta.testing.junit5.sql;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * If using an externally started / running DB instance, create a method to
 * get the JDBC URI to connect to the DB.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface DataSourceURIMethod {
    /**
     * @return Name of non-static public method returning the JDBC URI string
     *         value.
     */
    String value();
}
