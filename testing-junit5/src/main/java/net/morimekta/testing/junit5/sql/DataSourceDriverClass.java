package net.morimekta.testing.junit5.sql;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.sql.Driver;

/**
 * Specify the driver class to load for using the database. Only needed if
 * the driver class is not available using service lookup /
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface DataSourceDriverClass {
    /**
     * @return Name of non-static public method returning the JDBC URI string
     *         value.
     */
    Class<? extends Driver> value();
}
