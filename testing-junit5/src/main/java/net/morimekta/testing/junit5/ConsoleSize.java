package net.morimekta.testing.junit5;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static net.morimekta.testing.console.Console.DEFAULT_COLS;
import static net.morimekta.testing.console.Console.DEFAULT_ROWS;

/**
 * When using the {@link ConsoleExtension}, this will override the provided console size. Default
 * is 144 cols by 25 rows.
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface ConsoleSize {
    /**
     * @return Number of characters rows.
     */
    int rows() default DEFAULT_ROWS;

    /**
     * @return Number of characters columns.
     */
    int cols() default DEFAULT_COLS;
}
