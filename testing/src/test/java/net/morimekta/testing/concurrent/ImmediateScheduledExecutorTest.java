/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.testing.concurrent;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@SuppressWarnings({"FutureReturnValueIgnored", "PreferJavaTimeOverload"})
public class ImmediateScheduledExecutorTest {
    private FakeClock                  clock;
    private ImmediateScheduledExecutor executor;

    @BeforeEach
    public void setUp() {
        clock = new FakeClock();
        executor = new ImmediateScheduledExecutor(clock);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testSchedule() throws Exception {
        Callable<Long> r1 = mock(Callable.class);
        Runnable r2 = mock(Runnable.class);
        Callable<Long> r3 = mock(Callable.class);

        when(r1.call()).thenReturn(1L);
        when(r3.call()).thenReturn(3L);

        Future<Long> f1 = executor.schedule(r1, 10L, TimeUnit.MILLISECONDS);
        Future<?> f2 = executor.schedule(r2, 10L, TimeUnit.SECONDS);
        Future<Long> f3 = executor.schedule(r3, 10L, TimeUnit.MINUTES);

        assertThat(f1.isDone(), is(false));
        assertThat(f2.isDone(), is(false));
        assertThat(f3.isDone(), is(false));

        clock.tick(100L);

        assertThat(f1.isDone(), is(true));
        assertThat(f2.isDone(), is(false));
        assertThat(f3.isDone(), is(false));

        assertThat(f1.get(), equalTo(1L));

        clock.tick(100L, TimeUnit.SECONDS);

        assertThat(f1.isDone(), is(true));
        assertThat(f2.isDone(), is(true));
        assertThat(f3.isDone(), is(false));

        assertThat(f2.get(), nullValue());

        clock.tick(100L, TimeUnit.MINUTES);

        assertThat(f1.isDone(), is(true));
        assertThat(f2.isDone(), is(true));
        assertThat(f3.isDone(), is(true));

        assertThat(f3.get(), equalTo(3L));

        verify(r1).call();
        verify(r2).run();
        verify(r3).call();

        verifyNoMoreInteractions(r1, r2, r3);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testInvokeAll() throws Exception {
        Callable<String> a = mock(Callable.class);
        Callable<String> b = mock(Callable.class);

        when(a.call()).thenReturn("a");
        when(b.call()).thenReturn("b");

        List<Future<String>> l = executor.invokeAll(List.of(a, b), 10L, TimeUnit.MILLISECONDS);

        clock.tick(1);

        verify(a).call();
        verify(b).call();

        assertThat(l, hasSize(2));
        assertThat(l.get(0).get(), is("a"));
        assertThat(l.get(1).get(), is("b"));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testInvokeAny() throws Exception {
        Callable<String> a = mock(Callable.class);
        Callable<String> b = mock(Callable.class);
        Callable<String> c = mock(Callable.class);
        Callable<String> d = mock(Callable.class);

        when(a.call()).thenThrow(new IllegalArgumentException());
        when(b.call()).thenThrow(new IllegalStateException());
        when(c.call()).thenReturn("OK");

        String result = executor.invokeAny(Arrays.asList(a, b, c, d), 10, TimeUnit.MILLISECONDS);

        assertThat(result, is(equalTo("OK")));

        verify(a).call();
        verify(b).call();
        verify(c).call();
        verifyNoMoreInteractions(a, b, c, d);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testInvokeAny_failed() throws Exception {
        Callable<String> a = mock(Callable.class);
        Callable<String> b = mock(Callable.class);

        when(a.call()).thenThrow(new IllegalArgumentException());
        when(b.call()).thenThrow(new IllegalStateException());

        try {
            executor.invokeAny(Arrays.asList(a, b));
            fail("No exception");
        } catch (ExecutionException e) {
            assertThat(e.getMessage(), is(equalTo("All 2 tasks failed, first exception")));
            assertThat(e.getCause(), is(instanceOf(IllegalArgumentException.class)));
            assertThat(e.getSuppressed().length, is(1));
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testFakeTask() throws Exception {
        ImmediateScheduledExecutor.FakeTask<String> task
                = executor.schedule(() -> "a", 1000, TimeUnit.MILLISECONDS);

        ImmediateScheduledExecutor.FakeTask<String> other_same
                = executor.schedule(() -> "b", 1000, TimeUnit.MILLISECONDS);
        ImmediateScheduledExecutor.FakeRecurringTask recurring
                = executor.scheduleAtFixedRate(() -> {}, 1000, 10000, TimeUnit.MILLISECONDS);
        assertThat(task, is(task));
        assertThat(task, is(not(other_same)));
        assertThat(task, is(not(recurring)));

        assertThat(task.compareTo(other_same), is(-1));
        assertThat(task.compareTo(recurring), is(0));

        assertThat(task.getDelay(TimeUnit.MILLISECONDS), is(1000L));
        assertThat(task.get(), is("a"));
        assertThat(task.get(100L, TimeUnit.MILLISECONDS), is("a"));

        clock.tick(100L);

        assertThat(task.getDelay(TimeUnit.MILLISECONDS), is(0L));

        ScheduledFuture<String> task2 = executor.schedule(() -> "a", 1000, TimeUnit.SECONDS);
        try {
            task2.get(1000L, TimeUnit.MILLISECONDS);
            fail("no exception");
        } catch (TimeoutException e) {
            assertThat(e.getMessage(), is("Timed out after 1000 millis"));
        }

        assertThat(task2.cancel(true), is(true));
        assertThat(task2.isCancelled(), is(true));
        assertThat(task2.isDone(), is(true));
        try {
            task2.get(1000L, TimeUnit.MILLISECONDS);
            fail("no exception");
        } catch (InterruptedException e) {
            assertThat(e.getMessage(), is("Task cancelled"));
        }

        try {
            task2.get();
            fail("no exception");
        } catch (InterruptedException e) {
            assertThat(e.getMessage(), is("Task cancelled"));
        }

        Callable<String> runnable = mock(Callable.class);
        doThrow(new IOException("Boo")).when(runnable).call();

        ScheduledFuture<String> task3 = executor.schedule(runnable, 1000, TimeUnit.MILLISECONDS);
        clock.tick(2000, TimeUnit.MILLISECONDS);

        assertThat(task3.isDone(), is(true));
        assertThat(task3.isCancelled(), is(false));

        try {
            task3.get();
            fail("no exception");
        } catch (ExecutionException e) {
            assertThat(e.getMessage(), is("Boo"));
            assertThat(e.getCause(), instanceOf(IOException.class));
            assertThat(e.getCause().getMessage(), is("Boo"));
        }

        try {
            task3.get(100L, TimeUnit.MILLISECONDS);
            fail("no exception");
        } catch (ExecutionException e) {
            assertThat(e.getMessage(), is("Boo"));
            assertThat(e.getCause(), instanceOf(IOException.class));
            assertThat(e.getCause().getMessage(), is("Boo"));
        }

        assertThat(task, is(sameInstance(task)));
        assertThat(task, is(task));
        assertThat(task, is(not(task2)));
        assertThat(task.hashCode(), is(not(task2.hashCode())));
    }

    @Test
    public void testIllegalArgs() throws ExecutionException {
        Callable<?> callable = () -> null;
        Runnable runnable = () -> {};

        try {
            executor.awaitTermination(100, TimeUnit.MILLISECONDS);
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Shutdown not triggered"));
        }

        try {
            executor.schedule(callable, -100, TimeUnit.MILLISECONDS).get();
            fail("no exception");
        } catch (IllegalArgumentException | InterruptedException e) {
            assertThat(e.getMessage(), is("Unable to schedule tasks in the past"));
        }

        try {
            executor.scheduleAtFixedRate(runnable, -100, 100, TimeUnit.MILLISECONDS);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid initial delay or period: -100 / 100"));
        }

        try {
            executor.scheduleAtFixedRate(runnable, 100, -100, TimeUnit.MILLISECONDS);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid initial delay or period: 100 / -100"));
        }

        try {
            executor.scheduleWithFixedDelay(runnable, -100, 100, TimeUnit.MILLISECONDS);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid initial delay or intermediate delay: -100 / 100"));
        }

        try {
            executor.scheduleWithFixedDelay(runnable, 100, -100, TimeUnit.MILLISECONDS);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid initial delay or intermediate delay: 100 / -100"));
        }

        try {
            executor.invokeAll(List.of(), -3, TimeUnit.MILLISECONDS);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Negative timeout: -3"));
        }

        try {
            executor.invokeAny(List.of(), -3, TimeUnit.MILLISECONDS);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Negative timeout: -3"));
        }

        try {
            executor.invokeAll(List.of());
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty invoke collection"));
        }

        try {
            executor.invokeAny(List.of());
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty invoke collection"));
        }
    }

    @Test
    public void testShutdown() throws Exception {
        Callable<String> callable = () -> null;
        Runnable runnable = () -> {};

        executor.schedule(runnable, 10, TimeUnit.MILLISECONDS);
        executor.shutdown();

        assertThat(executor.isTerminated(), is(false));

        try {
            executor.schedule(callable, 100, TimeUnit.MILLISECONDS);
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Executor is shut down"));
        }

        try {
            executor.invokeAny(List.of(callable));
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Executor is shut down"));
        }

        try {
            executor.invokeAll(List.of(callable));
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Executor is shut down"));
        }

        try {
            executor.scheduleWithFixedDelay(runnable, 10, 10, TimeUnit.MILLISECONDS);
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Executor is shut down"));
        }

        try {
            executor.scheduleAtFixedRate(runnable, 10, 10, TimeUnit.MILLISECONDS);
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Executor is shut down"));
        }

        assertThat(executor.shutdownNow(), hasSize(1));
        assertThat(executor.isTerminated(), is(true));
    }

    @Test
    public void testAwaitTermination_immediate() {
        AtomicInteger calls = new AtomicInteger();
        Runnable runnable = calls::incrementAndGet;

        executor.execute(runnable);
        executor.submit(calls::incrementAndGet);
        executor.submit(runnable);
        executor.submit(runnable, "");
        executor.shutdown();

        assertThat(executor.isTerminated(), is(false));
        assertThat(executor.awaitTermination(10, TimeUnit.MILLISECONDS), is(true));
        assertThat(calls.get(), is(4));
        assertThat(executor.isTerminated(), is(true));
        assertThat(executor.shutdownNow(), hasSize(0));
    }

    @Test
    public void testAwaitTermination() {
        AtomicInteger calls = new AtomicInteger();
        Runnable runnable = calls::incrementAndGet;
        List<Future<?>> futures = new ArrayList<>();

        executor.submit(calls::incrementAndGet);
        futures.add(executor.submit(runnable));
        futures.add(executor.submit(runnable, ""));
        futures.add(executor.schedule(runnable, 10, TimeUnit.MILLISECONDS));
        futures.add(executor.schedule(runnable, 100, TimeUnit.MILLISECONDS));
        futures.add(executor.schedule(runnable, 1000, TimeUnit.MILLISECONDS));
        futures.add(executor.schedule(runnable, 10000, TimeUnit.MILLISECONDS));
        executor.shutdown();

        assertThat(futures, hasSize(6));
        assertThat(executor.isTerminated(), is(false));
        assertThat(executor.awaitTermination(5000, TimeUnit.MILLISECONDS), is(false));

        assertThat(calls.get(), is(6));

        assertThat(executor.isTerminated(), is(false));
        assertThat(executor.shutdownNow(), hasSize(1));
        assertThat(executor.isTerminated(), is(true));
    }

    @Test
    public void testScheduledAtFixedRate() throws InterruptedException {
        AtomicInteger calls = new AtomicInteger();
        Runnable task = () -> {
            calls.incrementAndGet();
            clock.tick(10);
        };

        ImmediateScheduledExecutor.FakeRecurringTask rate
                = executor.scheduleAtFixedRate(task, 100, 50, TimeUnit.MILLISECONDS);
        ImmediateScheduledExecutor.FakeRecurringTask other
                = executor.scheduleAtFixedRate(() -> {}, 100, 50, TimeUnit.MILLISECONDS);
        ImmediateScheduledExecutor.FakeTask<?> fake
                = executor.schedule(() -> "", 100, TimeUnit.MILLISECONDS);

        try {
            rate.get(1, TimeUnit.MILLISECONDS);
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Cannot wait for fake recurring tasks"));
        }

        assertThat(rate, is(rate));
        assertThat(rate, is(not(other)));
        assertThat(rate, is(not(new Object())));
        assertThat(rate.compareTo(other), is(-1));
        assertThat(rate.compareTo(fake), is(0));
        assertThat(rate.hashCode(), is(rate.hashCode()));
        assertThat(rate.isDone(), is(false));
        assertThat(rate.isCancelled(), is(false));

        clock.tick(210);

        assertThat(calls.get(), is(3));

        rate.cancel(true);

        assertThat(rate.isCancelled(), is(true));
        assertThat(rate.isDone(), is(true));

        other.cancel(false);

        clock.tick(100_000L);

        assertThat(calls.get(), is(3));

        try {
            rate.get();
            fail("no exception");
        } catch (InterruptedException e) {
            assertThat(e.getMessage(), is("Task cancelled"));
        }
    }

    @Test
    public void testScheduledWithFixedDelay() {
        AtomicInteger calls = new AtomicInteger();
        Runnable task = () -> {
            calls.incrementAndGet();
            clock.tick(10);
        };
        long start = clock.millis();

        Future<?> rate = executor.scheduleWithFixedDelay(task, 100, 50, TimeUnit.MILLISECONDS);
        Future<?> other = executor.schedule(() -> null, 10, TimeUnit.MILLISECONDS);

        clock.tick(210);

        assertThat(calls.get(), is(3));
        assertThat(clock.millis() - start, is(240L));

        assertThat(rate, is(not(other)));
    }
}
