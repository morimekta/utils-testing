/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.testing.concurrent;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@SuppressWarnings({"FutureReturnValueIgnored", "PreferJavaTimeOverload"})
public class ImmediateExecutorTest {
    private final ImmediateExecutor executor = new ImmediateExecutor();

    @Test
    public void testExecute() throws ExecutionException, InterruptedException {
        Runnable runnable = mock(Runnable.class);
        Runnable executed = mock(Runnable.class);

        Future<?> result = executor.submit(runnable);
        executor.execute(executed);

        verify(executed).run();
        verifyNoMoreInteractions(executed);
        verify(runnable).run();
        verifyNoMoreInteractions(runnable);

        assertThat(result.cancel(true), is(false));
        assertThat(result.isCancelled(), is(false));
        assertThat(result.isDone(), is(true));
        assertThat(result.get(), nullValue());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testDoneTask() throws Exception {
        Callable<String> callable = mock(Callable.class);

        when(callable.call()).thenReturn("a");

        Future<String> task = executor.submit(callable);

        assertThat(task.get(), is("a"));
        assertThat(task.isDone(), is(true));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testDoneTask_excepted() throws Exception {
        Callable<String> callable = mock(Callable.class);

        when(callable.call()).thenThrow(new IOException("e"));

        Future<String> task = executor.submit(callable);

        assertThat(task.isDone(), is(true));
        try {
            task.get(1, TimeUnit.MILLISECONDS);
            fail("no exception");
        } catch (ExecutionException e) {
            assertThat(e.getMessage(), is("java.io.IOException: e"));
            assertThat(e.getCause(), instanceOf(IOException.class));
            assertThat(e.getCause().getMessage(), is("e"));
        }
    }

    @Test
    public void testDoneTask_excepted2() throws Exception {
        Runnable callable = mock(Runnable.class);

        doThrow(new RuntimeException("e")).when(callable).run();

        Future<?> task = executor.submit(callable);

        assertThat(task.isDone(), is(true));
        try {
            task.get();
            fail("no exception");
        } catch (ExecutionException e) {
            assertThat(e.getMessage(), is("java.lang.RuntimeException: e"));
            assertThat(e.getCause(), instanceOf(RuntimeException.class));
            assertThat(e.getCause().getMessage(), is("e"));
        }
    }

    @Test
    public void testShutdown() throws ExecutionException {
        try {
            fail("no exception: " + executor.awaitTermination(1, TimeUnit.MILLISECONDS));
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Shutdown not triggered"));
        }

        executor.shutdown();

        assertThat(executor.isShutdown(), is(true));
        assertThat(executor.isTerminated(), is(true));
        assertThat(executor.awaitTermination(1, TimeUnit.MILLISECONDS), is(true));
        assertThat(executor.shutdownNow(), hasSize(0));

        try {
            fail("no exception: " + executor.submit(() -> {}));
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Executor is shut down"));
        }

        try {
            fail("no exception: " + executor.submit(() -> null));
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Executor is shut down"));
        }

        try {
            fail("no exception: " + executor.submit(() -> {}, ""));
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Executor is shut down"));
        }

        try {
            executor.execute(() -> {});
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Executor is shut down"));
        }

        try {
            fail("no exception: " + executor.invokeAll(List.of(() -> null), 1, TimeUnit.MILLISECONDS));
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Executor is shut down"));
        }

        try {
            fail("no exception: " + executor.invokeAny(List.of(() -> ""), 1L, TimeUnit.MILLISECONDS));
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Executor is shut down"));
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testInvokeAll() throws Exception {
        Callable<String> a = mock(Callable.class);
        Callable<String> b = mock(Callable.class);

        when(a.call()).thenReturn("a");
        when(b.call()).thenReturn("b");

        List<Future<String>> l = executor.invokeAll(List.of(a, b), 10L, TimeUnit.MILLISECONDS);

        verify(a).call();
        verify(b).call();

        assertThat(l, hasSize(2));
        assertThat(l.get(0).get(), is("a"));
        assertThat(l.get(1).get(), is("b"));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testInvokeAny() throws Exception {
        Callable<String> a = mock(Callable.class);
        Callable<String> b = mock(Callable.class);
        Callable<String> c = mock(Callable.class);
        Callable<String> d = mock(Callable.class);

        when(a.call()).thenThrow(new IllegalArgumentException());
        when(b.call()).thenThrow(new IllegalStateException());
        when(c.call()).thenReturn("OK");

        String result = executor.invokeAny(Arrays.asList(a, b, c, d), 10, TimeUnit.MILLISECONDS);

        assertThat(result, is(equalTo("OK")));

        verify(a).call();
        verify(b).call();
        verify(c).call();
        verifyNoMoreInteractions(a, b, c, d);

        try {
            executor.invokeAny(List.of());
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty invoke collection"));
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testInvokeAny_failed() throws Exception {
        Callable<String> a = mock(Callable.class);
        Callable<String> b = mock(Callable.class);

        when(a.call()).thenThrow(new IllegalArgumentException());
        when(b.call()).thenThrow(new IllegalStateException());

        try {
            executor.invokeAny(Arrays.asList(a, b));
            fail("No exception");
        } catch (ExecutionException e) {
            assertThat(e.getMessage(), is(equalTo("All 2 tasks failed, first exception")));
            assertThat(e.getCause(), is(CoreMatchers.instanceOf(IllegalArgumentException.class)));
            assertThat(e.getSuppressed().length, is(1));
        }
    }
}
