/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.testing.concurrent;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@SuppressWarnings("PreferJavaTimeOverload")
public class FakeScheduledExecutorTest {
    private FakeClock             clock;
    private FakeScheduledExecutor executor;

    @BeforeEach
    public void setUp() {
        clock = new FakeClock();
        executor = new FakeScheduledExecutor(clock);
    }

    @AfterEach
    public void tearDown() {
        executor.shutdown();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testSchedule() throws Exception {
        Callable<Long> r1 = mock(Callable.class);
        Runnable r2 = mock(Runnable.class);
        Callable<Long> r3 = mock(Callable.class);

        when(r1.call()).thenReturn(1L);
        when(r3.call()).thenReturn(3L);

        Future<Long> f1 = executor.schedule(r1, 10L, MILLISECONDS);
        Future<?> f2 = executor.schedule(r2, 10L, TimeUnit.SECONDS);
        Future<Long> f3 = executor.schedule(r3, 10L, TimeUnit.MINUTES);

        assertThat(f1.isDone(), is(false));
        assertThat(f2.isDone(), is(false));
        assertThat(f3.isDone(), is(false));

        clock.tick(100L);
        Thread.sleep(10);

        assertThat(f1.isDone(), is(true));
        assertThat(f2.isDone(), is(false));
        assertThat(f3.isDone(), is(false));

        assertThat(f1.get(1, SECONDS), equalTo(1L));

        clock.tick(100L, TimeUnit.SECONDS);
        Thread.sleep(10);

        assertThat(f1.isDone(), is(true));
        assertThat(f2.isDone(), is(true));
        assertThat(f3.isDone(), is(false));

        assertThat(f2.get(1, SECONDS), nullValue());

        clock.tick(100L, TimeUnit.MINUTES);
        Thread.sleep(10);

        assertThat(f1.isDone(), is(true));
        assertThat(f2.isDone(), is(true));
        assertThat(f3.isDone(), is(true));

        assertThat(f3.get(1, SECONDS), equalTo(3L));

        verify(r1).call();
        verify(r2).run();
        verify(r3).call();

        verifyNoMoreInteractions(r1, r2, r3);
    }

    @Test
    public void testScheduleException() throws InterruptedException {
        Future<?> f1 = executor.schedule((Runnable) () -> {
            throw new IllegalArgumentException("foo");
        }, 100L, MILLISECONDS);

        assertThat(f1.isDone(), is(false));

        clock.tick(100);
        Thread.sleep(10);

        assertThat(f1.isDone(), is(true));
        try {
            fail("no exception: " + f1.get());
        } catch (ExecutionException e) {
            assertThat(e.getCause(), is(instanceOf(IllegalArgumentException.class)));
            assertThat(e.getCause().getMessage(), is("foo"));
        }
    }

    @Test
    public void testScheduleAtFixedRate() throws InterruptedException, TimeoutException {
        AtomicInteger a = new AtomicInteger();
        AtomicInteger b = new AtomicInteger();

        ScheduledFuture<?> af = executor.scheduleAtFixedRate(
                a::incrementAndGet, 0, 100, MILLISECONDS);
        ScheduledFuture<?> bf = executor.scheduleAtFixedRate(
                () -> {
                    if (b.incrementAndGet() >= 20) {
                        throw new IllegalArgumentException("foo");
                    }
                }, 100, 10, MILLISECONDS);

        Thread.sleep(10);

        assertThat(a.get(), is(1));
        assertThat(b.get(), is(0));

        clock.tick(102);
        Thread.sleep(10);

        assertThat(a.get(), is(2));
        assertThat(b.get(), is(1));

        clock.tick(100);
        Thread.sleep(10);

        assertThat(a.get(), is(3));
        assertThat(b.get(), is(11));

        af.cancel(false);

        clock.tick(100);
        Thread.sleep(10);

        assertThat(a.get(), is(3));
        assertThat(b.get(), is(20));
        assertThat(bf.isDone(), is(true));
        try {
            fail("No exception: " + bf.get(1, SECONDS));
        } catch (ExecutionException e) {
            assertThat(e.getCause(), is(instanceOf(IllegalArgumentException.class)));
            assertThat(e.getCause().getMessage(), is("foo"));
        }

        clock.tick(100);
        Thread.sleep(10);

        assertThat(a.get(), is(3));
        assertThat(b.get(), is(20));
    }

    @Test
    public void testScheduleAtFixedDelay() throws InterruptedException, TimeoutException {
        AtomicInteger a = new AtomicInteger();
        AtomicInteger b = new AtomicInteger();

        ScheduledFuture<?> af = executor.scheduleWithFixedDelay(
                a::incrementAndGet, 0, 100, MILLISECONDS);
        ScheduledFuture<?> bf = executor.scheduleWithFixedDelay(
                () -> {
                    if (b.incrementAndGet() >= 4) {
                        throw new IllegalArgumentException("foo");
                    }
                }, 100, 50, MILLISECONDS);

        assertThat(a.get(), is(1));
        assertThat(b.get(), is(0));

        clock.tick(100);
        Thread.sleep(10);

        assertThat(a.get(), is(2));
        assertThat(b.get(), is(1));

        clock.tick(100);
        Thread.sleep(10);

        assertThat(a.get(), is(3));
        assertThat(b.get(), is(3));

        af.cancel(false);

        clock.tick(200);
        Thread.sleep(10);

        assertThat(a.get(), is(3));
        assertThat(b.get(), is(4));
        assertThat(bf.isDone(), is(true));
        try {
            fail("No exception: " + bf.get(1, SECONDS));
        } catch (ExecutionException e) {
            assertThat(e.getCause(), is(instanceOf(IllegalArgumentException.class)));
            assertThat(e.getCause().getMessage(), is("foo"));
        }

        clock.tick(100);
        Thread.sleep(10);

        assertThat(a.get(), is(3));
        assertThat(b.get(), is(4));
    }

    @Test
    public void testShutdown() throws InterruptedException, ExecutionException, TimeoutException {
        AtomicInteger a = new AtomicInteger();
        ScheduledFuture<?> af = executor.scheduleWithFixedDelay(
                a::incrementAndGet, 0, 100, MILLISECONDS);

        assertThat(a.get(), is(1));

        clock.tick(100);
        Thread.sleep(10);

        assertThat(a.get(), is(2));
        assertThat(executor.isShutdown(), is(false));
        assertThat(executor.isTerminated(), is(false));

        executor.shutdown();
        Thread.sleep(10);

        assertThat(af.isDone(), is(true));
        assertThat(af.get(1, SECONDS), is(nullValue()));

        clock.tick(100);
        Thread.sleep(10);

        assertThat(executor.awaitTermination(1, SECONDS), is(true));

        assertThat(a.get(), is(2));
        assertThat(executor.isShutdown(), is(true));
        assertThat(executor.isTerminated(), is(true));
    }

    @Test
    public void testShutdownNow() throws InterruptedException, ExecutionException, TimeoutException {
        AtomicInteger a = new AtomicInteger();
        ScheduledFuture<?> af = executor.scheduleWithFixedDelay(
                a::incrementAndGet, 0, 100, MILLISECONDS);

        Thread.sleep(10);

        assertThat(a.get(), is(1));

        clock.tick(100);
        Thread.sleep(10);

        assertThat(a.get(), is(2));
        assertThat(executor.isShutdown(), is(false));
        assertThat(executor.isTerminated(), is(false));

        assertThat(executor.shutdownNow(), hasSize(0));

        assertThat(af.isDone(), is(true));
        assertThat(af.get(1, SECONDS), is(nullValue()));

        clock.tick(100);
        Thread.sleep(10);

        assertThat(executor.awaitTermination(1, SECONDS), is(true));

        assertThat(a.get(), is(2));
        assertThat(executor.isShutdown(), is(true));
        assertThat(executor.isTerminated(), is(true));
    }
}
