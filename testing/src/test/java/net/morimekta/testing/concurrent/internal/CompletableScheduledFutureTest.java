/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.testing.concurrent.internal;

import net.morimekta.testing.concurrent.FakeClock;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicReference;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class CompletableScheduledFutureTest {
    @Test
    public void testCompletableScheduledFuture() {
        var clock = new FakeClock();
        var next1 = new AtomicReference<>(clock.instant());
        var next2 = new AtomicReference<>(clock.instant());
        var a = new CompletableScheduledFuture<Long>(clock, next1);
        var b = new CompletableScheduledFuture<Long>(clock, next2);

        assertThat(a, is(b));
        assertThat(a, is(not(new Object())));

        assertThat(a.hashCode(), is(b.hashCode()));
        assertThat(a.compareTo(b), is(0));

        next2.updateAndGet(it -> it.plusMillis(100L));
        a.complete(250L);

        assertThat(a.hashCode(), is(not(b.hashCode())));
        assertThat(a.compareTo(b), is(-1));
    }
}
