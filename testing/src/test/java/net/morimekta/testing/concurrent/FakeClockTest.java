/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.testing.concurrent;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static net.morimekta.testing.concurrent.FakeClock.forCurrentTimeMillis;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.quality.Strictness.WARN;

/**
 * Testing the fake clock.
 */
@MockitoSettings(strictness = WARN)
@ExtendWith(MockitoExtension.class)
@SuppressWarnings("PreferJavaTimeOverload")
public class FakeClockTest {
    @Test
    public void testFakeClock() {
        long now = System.currentTimeMillis();
        FakeClock clock = new FakeClock();
        FakeClock equal = forCurrentTimeMillis(clock.millis());
        FakeClock later = forCurrentTimeMillis(clock.millis() + 100);

        assertEquals(clock.millis(), now, 10D);
        assertEquals(clock, clock);
        assertEquals(clock, equal);
        assertNotEquals(clock, Clock.systemUTC());
        assertNotEquals(clock, clock.withZone(ZoneId.of("Europe/Oslo")));
        assertNotEquals(clock, later);

        assertEquals(clock.hashCode(), equal.hashCode());
        assertNotEquals(clock.hashCode(), later.hashCode());

        try {
            clock.tick(0);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid tick ms: 0"));
        }

        try {
            clock.tick(-2, TimeUnit.SECONDS);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid tick: -2 SECONDS"));
        }

        try {
            clock.tick(Duration.ofDays(-2));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid tick: -2 days"));
        }

        clock = FakeClock.forCurrentTimeMillis(1234567890_000L);
        var updatedInstant = new AtomicReference<>();
        clock.addListener(updatedInstant::set);
        assertThat(clock.toString(), is("FakeClock{@2009-02-13T23:31:30}"));
        clock.tick(100);
        assertThat(updatedInstant.get(), is(Instant.ofEpochMilli(1234567890_100L)));
    }

    @Test
    public void testForCurrentTimeMillis() {
        long now = System.currentTimeMillis();
        FakeClock clock = forCurrentTimeMillis(now);

        assertEquals(clock.millis(), now);
    }

    @Test
    public void testWithZone() {
        FakeClock utc = forCurrentTimeMillis(1234567890000L);
        FakeClock oslo = utc.withZone(ZoneId.of("Europe/Oslo"));
        ZonedDateTime nowUTC = ZonedDateTime.now(utc);
        ZonedDateTime nowOslo = ZonedDateTime.now(oslo);

        assertThat(nowUTC.withZoneSameInstant(oslo.getZone()), is(nowOslo));
        assertThat(nowUTC.getZone(), is(ZoneId.of("Z")));
        assertThat(nowOslo.getZone(), is(ZoneId.of("Europe/Oslo")));

        assertThat(oslo.instant(), is(utc.instant()));
        assertThat(oslo.millis(), is(utc.millis()));

        assertThat(oslo.withZone(ZoneId.of("Europe/Oslo")), is(sameInstance(oslo)));
    }

    @Test
    public void testTick() {
        FakeClock utc = forCurrentTimeMillis(1234567890000L);
        FakeClock oslo = utc.withZone(ZoneId.of("Europe/Oslo"));

        utc.tick(1234);

        assertThat(utc.millis(), is(1234567891234L));
        assertThat(utc.instant(), is(oslo.instant()));

        assertEquals(1234567891234L, utc.millis());
        assertEquals(1234567891234L, utc.instant().toEpochMilli());
        // Z, aka Zulu time, aka UTC.
        assertEquals("Z", utc.getZone().getId());

        assertThat(oslo.instant(), is(utc.instant()));

        ZonedDateTime nowUTC = ZonedDateTime.now(utc);
        ZonedDateTime nowOslo = ZonedDateTime.now(oslo);

        assertThat(nowUTC.withZoneSameInstant(oslo.getZone()), is(nowOslo));

        oslo.tick(1234, TimeUnit.SECONDS);

        assertEquals(1234569125234L, utc.millis());
        assertEquals(1234569125234L, utc.instant().toEpochMilli());
        // Z, aka Zulu time, aka UTC.
        assertEquals("Z", utc.getZone().getId());

        utc.tick(Duration.of(3, ChronoUnit.SECONDS));

        assertEquals(1234569128234L, utc.millis());
    }

    @Test
    public void testListeners() {
        FakeClock clock = forCurrentTimeMillis(1234567890000L);

        FakeClock.TimeListener l1 = mock(FakeClock.TimeListener.class);
        FakeClock.TimeListener l2 = mock(FakeClock.TimeListener.class);

        clock.addListener(l1);
        clock.addListener(l2);
        clock.addListener(l2);

        clock.tick(5L);

        verify(l1).getDelay(any());
        verify(l1).newCurrentTime(clock.instant());
        verify(l2).getDelay(any());
        verify(l2).newCurrentTime(clock.instant());
        verifyNoMoreInteractions(l1, l2);

        reset(l1, l2);

        clock.removeListener(l2);

        clock.tick(5L);

        verify(l1).getDelay(any());
        verify(l1).newCurrentTime(clock.instant());
        verifyNoMoreInteractions(l1, l2);
    }
}
