/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.testing.io;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import static java.nio.file.Files.readString;
import static net.morimekta.testing.io.ResourceUtil.copyResourceTo;
import static net.morimekta.testing.io.ResourceUtil.resourceAsString;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class ResourceUtilTest {
    @Test
    public void testCopyResourceTo(@TempDir Path tmp) throws IOException {
        var words = copyResourceTo("/net/morimekta/testing/text/words.txt", tmp);
        var other = copyResourceTo("/net/morimekta/testing/text/words.txt", tmp.resolve("other.txt"));

        assertThat(readString(words), is(readString(other)));

        try {
            fail("" + copyResourceTo("/net/morimekta/testing/text/does_not_exists.txt", tmp));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No such resource /net/morimekta/testing/text/does_not_exists.txt"));
        }
        try {
            fail("" + copyResourceTo("/net/morimekta/testing/text/words.txt", tmp));
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), startsWith("java.nio.file.FileAlreadyExistsException: /tmp/junit"));
            assertThat(e.getMessage(), endsWith("/words.txt"));
        }
        var words2 = copyResourceTo("/net/morimekta/testing/text/words.txt", tmp, StandardCopyOption.REPLACE_EXISTING);
        assertThat(words2, is(words));
    }

    @Test
    public void testResourceAsString() {
        assertThat(resourceAsString("/net/morimekta/testing/text/words.txt"), startsWith(
                "people 372 (noun)\n" +
                "history 187 (noun)\n" +
                "way 185 (noun)\n" +
                "art 183 (noun)\n"));
        try {
            fail("" + resourceAsString("/net/morimekta/testing/text/does_not_exists.txt"));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No such resource /net/morimekta/testing/text/does_not_exists.txt"));
        }
        try {
            fail("" + resourceAsString("/net/morimekta/testing/text/EnglishWords.class"));
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), startsWith("java.io.UnsupportedEncodingException: "));
        }
    }
}
