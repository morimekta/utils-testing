/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.testing.console;

import net.morimekta.io.tty.TTYSize;
import net.morimekta.strings.chr.Char;
import net.morimekta.strings.chr.CharReader;
import net.morimekta.strings.chr.CharUtil;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

import static java.util.concurrent.TimeUnit.SECONDS;
import static net.morimekta.strings.StringUtil.stripNonPrintable;
import static net.morimekta.strings.chr.Unicode.unicode;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class ConsoleManagerTest {
    @Test
    public void testDefaults() {
        ConsoleManager manager = new ConsoleManager();
        assertThat(manager.getConsole(), is(notNullValue()));
        assertThat(manager.getTTY(), is(notNullValue()));
        try {
            manager.doBeforeEach();

            Console console = manager.getConsole();
            assertThat(console.tty().getTerminalSize(), is(Console.DEFAULT_TERMINAL_SIZE));
            assertThat(console.tty().isInteractive(), is(true));
            assertThat(console.getConsoleIn(), is(notNullValue()));
            assertThat(console.getConsoleOut(), is(notNullValue()));
            assertThat(console.getConsoleErr(), is(notNullValue()));
        } finally {
            manager.doAfterEach();
        }
    }

    @Test
    public void testIoOverride() throws IOException {
        ConsoleManager manager = new ConsoleManager();
        try {
            manager.setTerminalSize(new TTYSize(20, 80));
            manager.doBeforeEach();

            Console console = manager.getConsole();
            System.err.write('+');
            System.err.println("foo");
            System.out.write('-');
            System.out.println("bar");
            assertThat(console.output(), is("-bar\n"));
            assertThat(console.output(), is("-bar\n"));
            assertThat(console.error(), is("+foo\n"));
            assertThat(console.error(), is("+foo\n"));
            console.reset();
            assertThat(console.output(), is(""));
            assertThat(console.error(), is(""));

            console.setInput(unicode('b'));

            assertThat(System.in.markSupported(), is(true));
            System.in.mark(1);
            assertThat(System.in.available(), is(1));
            assertThat((char) System.in.read(), is('b'));
            assertThat(System.in.available(), is(0));
            System.in.reset();
            assertThat((char) System.in.read(), is('b'));
            assertThat(System.in.read(), is(-1));

            assertThat(console.tty().getTerminalSize(), is(new TTYSize(20, 80)));
        } finally {
            manager.doAfterEach();
        }
    }

    @Test
    public void testOutputOnFailure() {
        ConsoleManager capture = new ConsoleManager();
        try {
            capture.setForkOutput(false);
            capture.setForkError(false);
            capture.doBeforeEach();

            ConsoleManager manager = new ConsoleManager();
            try {
                manager.doBeforeEach();
                manager.setDumpErrorOnFailure(true);
                manager.setDumpOutputOnFailure(true);
                manager.setForkError(false);
                manager.setForkOutput(false);
                System.err.write('+');
                System.err.println("foo");
                System.out.write('-');
                System.out.println("bar");
                manager.onTestFailed("testFailure");
            } finally {
                manager.doAfterEach();
            }

            assertThat(capture.getConsole().output(), is(""));
            assertThat(stripNonPrintable(capture.getConsole().error()),
                       is("" +
                          " <<< --- stdout : testFailure --- >>>\n" +
                          "-bar\n" +
                          " <<< --- stdout : END --- >>>\n" +
                          "\n" +
                          " <<< --- stderr : testFailure --- >>>\n" +
                          "+foo\n" +
                          " <<< --- stderr : END --- >>>\n"));
        } finally {
            capture.doAfterEach();
        }
    }

    @Test
    public void testInput() throws IOException {
        ConsoleManager manager = new ConsoleManager();
        try {
            manager.doBeforeEach();

            manager.getConsole().setInput(new byte[]{1, 2, 3, 4, 5});

            assertThat(System.in.skip(1), is(1L));
            byte[] buf = new byte[3];
            assertThat(System.in.read(buf), is(3));
            assertThat(buf, is(new byte[]{2, 3, 4}));
            assertThat(System.in.read(buf, 0, 3), is(1));
            assertThat(buf, is(new byte[]{5, 3, 4}));

            manager.getConsole().setInput(new byte[]{1, 2, 3, 4, 5});
            assertThat(System.in.read(buf), is(3));
            assertThat(buf, is(new byte[]{1, 2, 3}));
            System.in.close();
            assertThat(System.in.read(), is(-1));
        } finally {
            manager.doAfterEach();
        }
    }

    @Test
    public void testInteractiveInput() throws IOException, InterruptedException, ExecutionException, TimeoutException {
        ConsoleManager manager = new ConsoleManager();

        var read = new ArrayBlockingQueue<Char>(10);
        var executor = Executors.newSingleThreadExecutor();

        try {
            manager.doBeforeEach();

            OutputStream in = manager.getConsole().createInputSource();

            Future<Integer> done = executor.submit(() -> {
                try {
                    CharReader reader = new CharReader(System.in);
                    Char c;
                    int i = 0;
                    while ((c = reader.read()) != null) {
                        read.add(c);
                        ++i;
                    }
                    return i;
                } catch (IOException e) {
                    throw new UncheckedIOException(e);
                }
            });

            in.write(CharUtil.inputBytes("foo\n"));
            assertThat(read.poll(2, SECONDS), is(unicode('f')));
            assertThat(read.poll(1, SECONDS), is(unicode('o')));
            assertThat(read.poll(1, SECONDS), is(unicode('o')));
            assertThat(read.poll(1, SECONDS), is(unicode('\n')));
            assertThat(read.isEmpty(), is(true));
            in.write(CharUtil.inputBytes("bar"));
            in.close();

            assertThat(read.poll(2, SECONDS), is(unicode('b')));
            assertThat(read.poll(1, SECONDS), is(unicode('a')));
            assertThat(read.poll(1, SECONDS), is(unicode('r')));
            assertThat(read.isEmpty(), is(true));

            assertThat(done.get(1, SECONDS), is(7));
            assertThat(read.isEmpty(), is(true));
        } finally {
            manager.doAfterEach();
        }
    }

    @Test
    public void testNonInteractive() {
        ConsoleManager manager = new ConsoleManager();
        try {
            manager.setInteractive(false);
            manager.doBeforeEach();

            assertThat(manager.getTTY().isInteractive(), is(false));
            try {
                fail("" + manager.getTTY().getTerminalSize());
            } catch (UncheckedIOException e) {
                assertThat(e.getMessage(), is("java.io.IOException: Non-interactive test-console"));
            }
        } finally {
            manager.doAfterEach();
        }
    }
}
