/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.testing.file;

import net.morimekta.file.FileUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.jupiter.api.Assertions.fail;

public class TestConfigMapTest {
    @TempDir
    Path dir;

    @Test
    public void testConfigMap() throws IOException {
        var config = new TestConfigMap(dir);

        assertThat(config.getConfigMapDir(), is(dir));
        assertThat(getFileNames(dir), is(List.of()));
        assertThat(config.toString(), is("ConfigMap{}"));

        try (var update = config.update()) {
            update.copyResource("/test-resource.txt");
        }

        assertThat(getFileNames(dir), contains("test-resource.txt"));
        assertThat(config.toString(), is("ConfigMap{\"test-resource.txt\"}"));
        assertThat(config.getFile("test-resource.txt"),
                   is(config.getPath("test-resource.txt").toFile()));
        assertThat(Files.readString(config.getPath("test-resource.txt")),
                   is("This is a test resource."));

        try (var update = config.update()) {
            update.copyResource("test-resource.txt", "/other-test-resource.txt");
        }

        assertThat(getFileNames(dir), contains("test-resource.txt"));
        assertThat(Files.readString(config.getPath("test-resource.txt")),
                   is("This is a different test resource."));

        try (var update = config.update()) {
            update.writeContent("original-resource.txt", "This is a great test resource.");
            assertThat(Files.exists(update.path("original-resource.txt")), is(true));
            assertThat(update.file("original-resource.txt").exists(), is(true));
        }

        assertThat(config.toString(), is("ConfigMap{\"original-resource.txt\",\"test-resource.txt\"}"));
        assertThat(getFileNames(dir), contains("original-resource.txt", "test-resource.txt"));
        assertThat(Files.readString(config.getPath("original-resource.txt")),
                   is("This is a great test resource."));
        assertThat(Files.readString(config.getPath("test-resource.txt")),
                   is("This is a different test resource."));

        try (var update = config.update()) {
            assertThat(update.delete("test-resource.txt"), is(true));
            assertThat(update.delete("does-not-exists.txt"), is(false));

            try {
                fail("No exception" + update.path(".hidden"));
            } catch (IllegalArgumentException e) {
                assertThat(e.getMessage(), is("Invalid config map file name: \".hidden\""));
            }
        }
        assertThat(getFileNames(dir), contains("original-resource.txt"));

        try {
            try (var update = config.update()) {
                update.path("do-not-make");
            }
            fail("No exception on file not created.");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("No file created for 'do-not-make'."));
        }
    }

    private static List<String> getFileNames(Path dir) throws IOException {
        return FileUtil.list(dir)
                       .stream()
                       .filter(f -> {
                           try {
                               return !Files.isHidden(f);
                           } catch (IOException e) {
                               throw new UncheckedIOException(e);
                           }
                       })
                       .map(Path::getFileName)
                       .map(Path::toString)
                       .sorted()
                       .collect(Collectors.toList());
    }
}
