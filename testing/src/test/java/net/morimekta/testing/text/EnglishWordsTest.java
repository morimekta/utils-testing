/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.testing.text;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.greaterThan;

public class EnglishWordsTest {
    @Test
    public void testWord() {
        assertThat(EnglishWords.word().length(), is(notNullValue()));
    }

    @Test
    public void testSentence() {
        for (int i = 25; i < 100; i += 5) {
            assertThat(EnglishWords.sentence(i + 1).length(), is(greaterThan(i)));
        }
    }

    @Test
    public void testParagraph() {
        String paragraph = EnglishWords.paragraph(3);
        String[] sentences = paragraph.split("[.]");
        assertThat(sentences.length, is(3));
    }

    @Test
    public void testCicero() {
        assertThat(EnglishWords.cicero(), is(
                "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium\n"
                + "doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore\n"
                + "veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim\n"
                + "ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia\n"
                + "consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque\n"
                + "porro quisquam est, qui dolorem ipsum, quia dolor sit amet consectetur\n"
                + "adipisci velit, sed quia non numquam do eius modi tempora incididunt, ut labore\n"
                + "et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis\n"
                + "nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea\n"
                + "commodi consequatur? Quis autem vel eum irure reprehenderit, qui in ea voluptate\n"
                + "velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat,\n"
                + "quo voluptas nulla pariatur?\n"
                + "\n"
                + "At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis\n"
                + "praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias\n"
                + "excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui\n"
                + "officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem\n"
                + "rerudum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis\n"
                + "est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat\n"
                + "facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.\n"
                + "Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus\n"
                + "saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae.\n"
                + "Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis\n"
                + "voluptatibus maiores alias consequatur aut perferendis doloribus asperiores\n"
                + "repellat.\n"));
    }

    @Test
    public void testLoremIpsum() {
        assertThat(EnglishWords.loremIpsum(), is(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\n"
                + "incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis\n"
                + "nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n"
                + "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu\n"
                + "fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in\n"
                + "culpa qui officia deserunt mollit anim id est laborum.\n"));
    }

    @Test
    public void testLoremIpsumOneline() {
        assertThat(EnglishWords.loremIpsumOneline(), is(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor "
                + "incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis "
                + "nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
                + "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu "
                + "fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in "
                + "culpa qui officia deserunt mollit anim id est laborum."));
    }
}
