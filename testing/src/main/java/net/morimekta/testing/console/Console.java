/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.testing.console;

import net.morimekta.io.tty.TTY;
import net.morimekta.io.tty.TTYSize;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Base interface for accessing console during testing. Implementation in framework specific
 * modules.
 */
public interface Console {
    /**
     * @return The testing TTY. This is essentially a fake TTY that reflects back the state of the testing console.
     */
    TTY tty();

    /**
     * @return Get the normal output.
     */
    String output();

    /**
     * @return Get the error output.
     */
    String error();

    /**
     * Set input to return the given bytes.
     *
     * @param in The bytes for input.
     */
    void setInput(byte[] in);

    /**
     * Set input with dynamic content.
     *
     * @param in The input values.
     */
    void setInput(Object... in);

    /**
     * Create an output stream that will feed the input stream. Handy
     * for testing interactive input.
     *
     * @return The controlling output stream.
     */
    OutputStream createInputSource();

    /**
     * @return The input stream that the console will set to System.in.
     */
    InputStream getConsoleIn();

    /**
     * @return The print stream that the console will set to System.out.
     */
    PrintStream getConsoleOut();

    /**
     * @return The printr stream that the console will set to System.err.
     */
    PrintStream getConsoleErr();

    /**
     * Reset all the streams for the console.
     */
    void reset();

    /** The default number of rows in a fake TTY. */
    int     DEFAULT_ROWS          = 25;
    /** The default number of cols in a fake TTY. */
    int     DEFAULT_COLS          = 144;
    /** The default terminal size of a fake TTY. */
    TTYSize DEFAULT_TERMINAL_SIZE = new TTYSize(DEFAULT_ROWS, DEFAULT_COLS);
}
