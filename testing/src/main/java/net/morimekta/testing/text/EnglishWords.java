/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.testing.text;

import static net.morimekta.testing.text.EnglishWordsInstance.getInstance;

/**
 * This is a very light-weight version of fairy, which just generates
 * simple pseudo-sentences in repeated SVO patterns.
 */
public final class EnglishWords {
    /**
     * @return A single english word.
     */
    public static String word() {
        return getInstance().word();
    }

    /**
     * @param minLength Minimum length in characters.
     * @return A sentence of english words. Terminated with period '.'.
     */
    public static String sentence(int minLength) {
        return getInstance().sentence(minLength);
    }

    /**
     * @param sentences Number of sentences in the paragraph.
     * @return A paragraph of text.
     */
    public static String paragraph(int sentences) {
        return getInstance().paragraph(sentences);
    }

    /**
     * The standard 1-paragraph "Lorem Ipsum" text. It contains newlines, so if
     * a oneline text is wanted use the {@link #loremIpsumOneline()} function.
     *
     * @return The standard 1-paragraph "Lorem Ipsum" text.
     */
    public static String loremIpsum() {
        return getInstance().loremIpsum();
    }

    /**
     * The standard 1-paragraph "Lorem Ipsum" text as a single line text.
     *
     * @return The standard 1-paragraph "Lorem Ipsum" text.
     */
    public static String loremIpsumOneline() {
        return getInstance().loremIpsumOneline();
    }

    /**
     * Get the original text from which "Lorem Ipsum" is based on, from §1.10.33
     * and §1.10.34 of the "De finibus bonorum et malorum" manuscript, which is
     * a Socratian dialogue debating the differences between three different
     * popular philosophies of the time. It was written by Cicero and first
     * published in 45 BCE. The modern version of "Lorem Ipsum" contains excepts
     * (and mistypings) from the two paragraphs in the text provided here.
     * <p>
     * The text here has been slightly modified to make a simple text without
     * optional letters from different transcripts, or possibly mistypings,
     * where I've chosen by random whether to include it or not.
     *
     * @return The Cicero text from which Lorem Ipsum is based on.
     */
    public static String cicero() {
        return getInstance().cicero();
    }

    // --- Private ---

    private EnglishWords() {}
}
