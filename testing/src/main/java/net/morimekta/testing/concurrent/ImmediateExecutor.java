/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.testing.concurrent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.requireNonNull;

/**
 * Fake executor service that runs all tasks immediately. If you need a fake
 * executor that delays execution, use {@link ImmediateScheduledExecutor}, and
 * trigger executions by ticking the clock.
 */
public class ImmediateExecutor implements ExecutorService {
    /**
     * Create an immediate executor.
     */
    public ImmediateExecutor() {}

    @Override
    public void shutdown() {
        this.shutdownCalled = true;
    }

    @Override
    public List<Runnable> shutdownNow() {
        this.shutdownCalled = true;
        return Collections.emptyList();
    }

    @Override
    public boolean isShutdown() {
        return shutdownCalled;
    }

    @Override
    public boolean isTerminated() {
        return shutdownCalled;
    }

    @Override
    public boolean awaitTermination(long l, TimeUnit timeUnit) {
        requireNonNull(timeUnit, "timeUnit == null");
        if (!shutdownCalled) {
            throw new IllegalStateException("Shutdown not triggered");
        }
        return true;
    }

    @Override
    public <T> Future<T> submit(Callable<T> callable) {
        requireNonNull(callable, "callable == null");
        if (isShutdown()) {
            throw new IllegalStateException("Executor is shut down");
        }
        try {
            return CompletableFuture.completedFuture(callable.call());
        } catch (Exception e) {
            return CompletableFuture.failedFuture(e);
        }
    }

    @Override
    public <T> Future<T> submit(Runnable runnable, T t) {
        requireNonNull(runnable, "runnable == null");
        if (isShutdown()) {
            throw new IllegalStateException("Executor is shut down");
        }
        try {
            runnable.run();
        } catch (Exception e) {
            return CompletableFuture.failedFuture(e);
        }
        return CompletableFuture.completedFuture(t);
    }

    @Override
    public Future<?> submit(Runnable runnable) {
        return submit(runnable, null);
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection) {
        requireNonNull(collection, "collection == null");
        if (isShutdown()) {
            throw new IllegalStateException("Executor is shut down");
        }

        List<Future<T>> results = new ArrayList<>();
        for (Callable<T> c : collection) {
            results.add(submit(c));
        }
        return results;
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection, long l, TimeUnit timeUnit) {
        requireNonNull(timeUnit, "timeUnit == null");
        return invokeAll(collection);
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> collection) throws ExecutionException {
        requireNonNull(collection, "collection == null");
        if (isShutdown()) {
            throw new IllegalStateException("Executor is shut down");
        }

        ExecutionException ex = null;
        for (Callable<T> c : collection) {
            try {
                return c.call();
            } catch (Exception e) {
                if (ex == null) {
                    ex = new ExecutionException("All " + collection.size() + " tasks failed, first exception", e);
                } else {
                    ex.addSuppressed(e);
                }
            }
        }
        if (ex == null) {
            // because the collection was empty.
            throw new IllegalArgumentException("Empty invoke collection");
        }
        throw ex;
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> collection, long l, TimeUnit timeUnit)
            throws ExecutionException {
        requireNonNull(timeUnit, "timeUnit == null");
        return invokeAny(collection);
    }

    @Override
    @SuppressWarnings("FutureReturnValueIgnored")
    public void execute(Runnable runnable) {
        submit(runnable, null);
    }

    // ---- Private ----

    private boolean shutdownCalled = false;
}
