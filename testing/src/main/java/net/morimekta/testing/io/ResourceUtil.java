/*
 * Copyright (c) 2020, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.testing.io;

import net.morimekta.strings.ReaderUtil;
import net.morimekta.strings.io.Utf8StreamReader;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Simple utility helping with using resources in testing.
 */
public final class ResourceUtil {
    /**
     * Copy resource to target location. If the destination is a directory, then
     * copy it as a file with the same file name as the original resource to that
     * directory (same as for 'cp').
     *
     * @param resource The resource name.
     * @param target   The target location.
     * @param options  Copy options.
     * @return The file path of the copied resource.
     */
    public static Path copyResourceTo(String resource, Path target, CopyOption... options) {
        return copyResourceTo(getCallingMethodClass(), resource, target, options);
    }

    /**
     * Copy resource to target location. If the destination is a directory, then
     * copy it as a file with the same file name as the original resource to that
     * directory (same as for 'cp').
     *
     * @param klass    Class context to load resource.
     * @param resource The resource name.
     * @param target   The target location.
     * @param options  Copy options.
     * @return The file path of the copied resource.
     */
    public static Path copyResourceTo(Class<?> klass, String resource, Path target, CopyOption... options) {
        if (Files.isDirectory(target)) {
            var fileName = Paths.get(resource).getFileName().toString();
            target = target.resolve(fileName);
        }
        try (var in = klass.getResourceAsStream(resource)) {
            if (in == null) {
                throw new IllegalArgumentException("No such resource " + resource);
            }
            Files.copy(in, target, options);
            return target;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * @param resource A resource name.
     * @return The resource content as a string.
     */
    public static String resourceAsString(String resource) {
        return resourceAsString(getCallingMethodClass(), resource);
    }

    /**
     * @param klass    Class context to load resource.
     * @param resource A resource name.
     * @return The resource content as a string.
     */
    public static String resourceAsString(Class<?> klass, String resource) {
        try (var in = klass.getResourceAsStream(resource)) {
            if (in == null) {
                throw new IllegalArgumentException("No such resource " + resource);
            }
            return ReaderUtil.readAll(new Utf8StreamReader(in));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private ResourceUtil() {
    }

    @SuppressWarnings("unchecked")
    private static Class<?> getCallingMethodClass() {
        return StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE)
                          .walk(frameStream -> frameStream
                                  .map(frame -> (Class<Object>) frame.getDeclaringClass())
                                  .filter(type -> !type.equals(ResourceUtil.class))
                                  .findFirst())
                          .orElse((Class<Object>) ResourceUtil.class.asSubclass(Object.class));
    }
}
