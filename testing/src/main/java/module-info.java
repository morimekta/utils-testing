/**
 * Module with general testing utilities.
 */
module net.morimekta.testing {
    exports net.morimekta.testing.concurrent;
    exports net.morimekta.testing.console;
    exports net.morimekta.testing.file;
    exports net.morimekta.testing.io;
    exports net.morimekta.testing.text;

    opens net.morimekta.testing.file;

    requires transitive net.morimekta.io;

    requires net.morimekta.file;
    requires net.morimekta.strings;
}